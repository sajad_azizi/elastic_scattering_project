#!/usr/bin/python

#input reader

def in_reader( file_obj ):
    lines = file_obj.readlines()
    for n, line in enumerate(lines):
        if "Density" in line:
            density = float(line.split()[1])
        elif "Material" in line:
            material = line.split()[1]
        elif "N_Samples" in line:
            n_samples = int(float(line.split()[1]))
        elif "Energy_Cutoff" in line:
            e_cutoff = float(line.split()[1])
        elif "Energy" in line:
            e_initial = float(line.split()[1])
        elif "Width" in line:
            width = float(line.split()[1])


    return ( material, density, width, n_samples, e_initial, e_cutoff ) 

#Dictionary of data.
'''
 Dictionary that contains the information necesary to compute the mean free path, 
 that is in ordered fashion, microscopic elastic scattering cross section and atomic mass number.  
 It's accesed by using the simbol of the pure material as key. Only 'C' (C-12) , 'Ni'(Ni-58) that  and 'O'(O-16)
 are defined so far. Values taken from https://www.ncnr.nist.gov/resources/n-lengths/.
'''
material_dict = {'C':(5.551,12.011),'Ni':(26.1,57.9353429), 'O':(4.232,15.994914)}

import ctypes as C
import matplotlib.pyplot as plt
import sys
import numpy as np

clib = C.CDLL('./libNeutronScattering.so')
#print(sys.argv[1])
input_file = open( sys.argv[1], 'r' )

#input reader

material, density, width, n_samples, e_initial, e_cutoff = in_reader( input_file )

#process input data

micro_cross_sect, mass = material_dict[material]
mean_free_path = 1.0/(density*0.6022*micro_cross_sect/mass)

#call updater

clib.updater.restype = C.POINTER(C.c_double)
clib.updater.argtypes = [ C.c_int, C.c_float, C.c_double, C.c_double, C.c_double, C.c_double ]

energy_list = clib.updater( n_samples, width, mean_free_path, e_initial, mass, e_cutoff)

#very ugly way to get a nice numpy array. MUST CORRECT!!!
Energy_list = np.array([energy_list[i] for i in range(n_samples) if energy_list[i] != 0.0 ])

plt.hist(Energy_list, bins=100)
plt.show()

