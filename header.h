#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/**
 * @file header.h
 * @author doxygen Sajad Azizi
 * @date 18 Mar 2016
 * 
 * @brief File containing example of doxygen usage for quick reference.
 *
 * Here typically goes a more extensive explanation of what the header
 * defines. Doxygens tags are words preceeded by either a backslash @\
 * or by an at symbol @@.
 */


/**
 * @brief A simple stub function to show how links do work.
 *
 * Links are generated automatically for webpages (like http://www.google.com)
 * and for structures, like BoxStruct_struct.
 */



/* MACRO to get a random number in X in [0-1] */
/**
 *@param RN this is a random number gnerateor in computer
 * and also this is an integer RNG and uniform
*/
#define RN ((double)rand()/(RAND_MAX))

/* Particle structure definition */
struct Particle{
 /**
   * @param X is an double varitional
 */   
  double X;     /* X position */
};
struct Str_result{
 /**
   * @param Number_of_collision is an integer varitional and pointer which return Number of collision it define in updater function
 */
    int *Number_of_collision;
  /**
   * @param Efinal is an double varitional and pointer which return final energy it define in updater function
 */   
    double *Efinal;
};


/* function prototypes */
/**
 * this function have many argment in input and return tow value final energy and Number of collision
 * @param [in] particle_number this is an integer number that indicate number of Particle My mean in this project number of neutron
 * @param material_width this is a fload varitional that indicate to with of the material
 * @param mean_path this is a double varitional that indicate to mean free path of the material
 * @param Ei this a double varitional that indicate to initial energy of the neutron
 * @param A this a double varitional that indicate to Mass number of the material
 * @param E_thr this a double varitional that indicate to cut off energy or Threshold energy
*/
struct Str_result  updater(const int, const float, const double, const double, const double, const double);
/**
 * this function have 2 input and return displacement that in updater function update distance in each event
 * @param  random_number this is a random number generate uniform between [0,1]
 * @param mean_path this ia mean free path that independ to matrial and different for each matrial
*/
double displacement(const double, const double);
/**
 * this function have 1 input and return angle that in updater function update angle in each event
 * @param  random_number this is a random number generate uniform between [0,1]
*/
double angle(const double);
/**
 * this function have 3 argment in input and return final energy and update energy in each event
 * @param Ei this a double varitional that indicate to initial energy of the neutron
 * @param A this a double varitional that indicate to Mass number of the material
 * @param thetaL this a double varitional that indicate to angel in lab coordinate system
*/
double final_energy(const double, const double, const double);
/**
 * this function is a void function and dont return any value only deallocate tow varitional that we define in Str_result struct
 * @param result this is a varitional from pointer and an object from Str_result struct and free memory
*/
void dealloc(struct Str_result *result);

#endif /* HEADER_H */
