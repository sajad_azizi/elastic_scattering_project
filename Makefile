CC       = gcc
OPTIMIZE = -O3
DEBUG    = -Wall -ggdb
LIB      = -lm
PROG     = IronChampions.x
SHARED   = -fPIC

SOURCES = IronChampions.c functions.c final_energy.c
OBJECTS = $(SOURCES:.cpp=.o)

.PHONY: all clean

all: $(SOURCES) $(OBJECTS) $(PROG)

$(PROG): IronChampions.o Makefile header.h libNeutronScattering.so
	$(CC) $(DEBUG) $(OPTIMIZE) $(OBJECTS) -o $@ -L. -lNeutronScattering -Wl,-rpath,. $(LIB)

%.o: %.c Makefile
	$(CC) $(DEBUG) $(SHARED) $(OPTIMIZE) -c $< -o $@ $(LIB)

test: $(PROG) Makefile
	echo "\n testing ...\n"
	./$< 10
	echo "... ooooOOOOOooooo ... done ... ooooOOOOooooo\n"

libNeutronScattering.so: functions.o final_energy.o
	$(CC) -shared $^ -o $@ $(LIB)

clean:
	rm -rf *.x *~ *.o *.so
